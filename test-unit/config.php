<?php
$ds = DIRECTORY_SEPARATOR;
$config = \Pantagruel74\Yii2TestApp\YiiDefaultConfig::getBase(__DIR__);
$config['components']['dotEnvParser'] = [
    'class' => \Pantagruel74\Yii2DotenvParser\DotEnvParser::class,
    'path' => __DIR__ . $ds . '.env',
];
return $config;