<?php

namespace Pantagruel74\Yii2DotenvParserTestUnit;

use Pantagruel74\Yii2TestAppTestHelpers\AbstractBaseTest;

class DotEnvParserTest extends AbstractBaseTest
{
    /**
     * @return array
     */
    protected function getConfig(): array
    {
        $ds = DIRECTORY_SEPARATOR;
        return include __DIR__ . $ds . 'config.php';
    }

    /**
     * @return void
     * @throws \ErrorException
     */
    protected function testScenario(): void
    {
        $param1 = \Yii::$app->dotEnvParser->getVariable('PARAM1');
        $param2 = \Yii::$app->dotEnvParser->getVariable('PARAM2');
        $this->assertEquals('val1', $param1);
        $this->assertEquals('aboba', $param2);
    }
}