<?php

namespace Pantagruel74\Yii2DotenvParser;

use yii\base\Component;

class DotEnvParser extends Component
{
    public string $path = '';
    private array $envs = [];

    /**
     * @return void
     * @throws \ErrorException
     */
    public function init()
    {
        $this->checkPath();
        $this->envs  = $this->parseEnv($this->getFileData());
    }

    /**
     * @return void
     * @throws \ErrorException
     */
    protected function checkPath(): void
    {
        $ds = DIRECTORY_SEPARATOR;
        if(mb_substr($this->path, strlen($this->path) - 4) !== '.env') {
            throw new \ErrorException("Can't read .env file");
        }
    }

    /**
     * @return string
     * @throws \ErrorException
     */
    protected function getFileData(): string
    {
        $fileContent = file_get_contents($this->path);
        if($fileContent === false) {
            throw new \ErrorException("Can't read .env file");
        }
        return $fileContent;
    }

    /**
     * @param string $data
     * @return string[]
     * @throws \ErrorException
     */
    protected function parseEnv(string $data): array
    {
        $data = str_replace("\r\n", "\n", $data);
        $result = [];
        foreach (explode("\n", $data) as $str)
        {
            $parts = explode("=", $str, 2);
            if(count($parts) > 2) {
                throw new \ErrorException("String parse error: find more then 2 '=' symbols in string: '"
                    . $str . "'");
            }
            $result[$parts[0]] = $parts[1];
        }
        return $result;
    }

    /**
     * @param string $varName
     * @return string
     * @throws \ErrorException
     */
    public function getVariable(string $varName): string
    {
        if(!isset($this->envs[$varName])) {
            throw new \ErrorException("Variable " . $varName . " had not defined in .env");
        }
        return $this->envs[$varName];
    }
}